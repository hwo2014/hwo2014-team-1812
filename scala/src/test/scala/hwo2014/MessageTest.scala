package hwo2014

import org.json4s._
import org.json4s.native.JsonMethods._
import scala.Some
import akka.actor.{ActorSystem, Props}
import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * Created by beisenmenger on 16/04/2014.
 */
object MessageTest extends App {
  implicit val formats = new DefaultFormats{}


   val carPositions = """{"msgType": "carPositions", "data": [
                         {
                           "id": {
                             "name": "Schumacher",
                             "color": "red"
                           },
                           "angle": 0.0,
                           "piecePosition": {
                             "pieceIndex": 0,
                             "inPieceDistance": 0.0,
                             "lane": {
                               "startLaneIndex": 0,
                               "endLaneIndex": 0
                             },
                             "lap": 0
                           }
                         },
                         {
                           "id": {
                             "name": "Rosberg",
                             "color": "blue"
                           },
                           "angle": 45.0,
                           "piecePosition": {
                             "pieceIndex": 0,
                             "inPieceDistance": 20.0,
                             "lane": {
                               "startLaneIndex": 1,
                               "endLaneIndex": 1
                             },
                             "lap": 0
                           }
                         }
                       ], "gameId": "OIUHGERJWEOI", "gameTick": 0}"""


  testDecode(carPositions)

  val gameInitJson = """{"msgType": "gameInit", "data": {
                         "race": {
                           "track": {
                             "id": "indianapolis",
                             "name": "Indianapolis",
                             "pieces": [
                               {
                                 "length": 100.0
                               },
                               {
                                 "length": 100.0,
                                 "switch": true
                               },
                               {
                                 "radius": 200,
                                 "angle": 22.5
                               }
                             ],
                             "lanes": [
                               {
                                 "distanceFromCenter": -20,
                                 "index": 0
                               },
                               {
                                 "distanceFromCenter": 0,
                                 "index": 1
                               },
                               {
                                 "distanceFromCenter": 20,
                                 "index": 2
                               }
                             ],
                             "startingPoint": {
                               "position": {
                                 "x": -340.0,
                                 "y": -96.0
                               },
                               "angle": 90.0
                             }
                           },
                           "cars": [
                             {
                               "id": {
                                 "name": "Schumacher",
                                 "color": "red"
                               },
                               "dimensions": {
                                 "length": 40.0,
                                 "width": 20.0,
                                 "guideFlagPosition": 10.0
                               }
                             },
                             {
                               "id": {
                                 "name": "Rosberg",
                                 "color": "blue"
                               },
                               "dimensions": {
                                 "length": 40.0,
                                 "width": 20.0,
                                 "guideFlagPosition": 10.0
                               }
                             }
                           ],
                           "raceSession": {
                             "laps": 3,
                             "maxLapTimeMs": 30000,
                             "quickRace": true
                           }
                         }
                       }}"""

  testDecode(gameInitJson)

  val system = ActorSystem("scarla")
  var blackbox = system.actorOf(Props(new BlackBox(5 seconds)))

  blackbox ! decode(parse(carPositions)).get
  blackbox ! Replay()

  def testDecode(line:String) {

    val json = parse(line)

    decode(json) match {
      case Some(x) => println(x)
      case None => println("Unknown: " + json)
    }
  }


  def decode(json:JValue):Option[RaceMessage] = {
      (json \ "msgType").extract[String] match {
        case "carPositions" => Some(json.extract[CarPositions])
        case "lapFinished" => Some(json.extract[LapFinished])
        case "spawn" => Some(json.extract[Spawn])
        case "crash" => Some(json.extract[Crash])
        case "gameEnd" => Some(json.extract[GameEnd])
        case "throttle" => Some(json.extract[Throttle])
        case "gameStart" => Some(json.extract[GameStart])
        case "gameInit" => Some(json.extract[GameInit])
        case _ =>
          None
      }
    }

}
