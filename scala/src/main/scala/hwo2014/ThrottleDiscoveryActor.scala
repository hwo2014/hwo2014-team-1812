package hwo2014

import akka.actor.{ActorRef, Props, Actor, ActorLogging}

/**
 * Created by beisenmenger on 22/04/2014.
 */
class ThrottleDiscoveryActor(car:ActorRef) extends Actor with ActorLogging {

  var lastPosition:CarPosition = null
      var speed = 0.0   // distance units per tick
      var acceleration = 10.0
      var maxSpeed = 0.0
      var maxAcceleration = 0.0
      var maxDeceleration = 1.0

    var slipVelocity = 0.0
    var slipAcceleration = 0.0

    var angularVelocity = 0.0
    var angularAcceleration = 0.0

    var throttle = 0.5

    var targetMonitor = context.actorOf(Props(new PieceTargetMonitor()), "targetMonitor")
    var turboAvailable = false
    var turboOn = false
    var turboInfo:TurboAvailable = null
    var crashed = false

    var dimensions:CarDimensionsMsg = null

    var initialThrottle = 0.575
    var force = 0.0

    var minForceAngle = 10.0


    override def receive = {
      case cd:CarDimensionsMsg => dimensions = cd
      case c:Crash =>
        targetMonitor ! c
        crashed = true
      case s:Spawn => crashed = false
      case gi:GameInit => targetMonitor ! gi
      case ta:TurboAvailable =>
        turboAvailable = true
        turboInfo = ta
      case t:TurboStart => turboOn = true
      case t:TurboEnd => turboOn = false
      case position:CarPosition =>
        val currentPiece = Track.pieces(position.piecePosition.pieceIndex)

        if (lastPosition != null) {
          val currentSpeed = Track.distanceBetween(lastPosition.piecePosition, position.piecePosition)

          if (currentSpeed > 20) {
            println("LAST PIECE: " + lastPosition.piecePosition)
            println("THIS PIECE: " + position.piecePosition)
          }

          if (currentSpeed > 100 || currentSpeed < 0) {

            log.info(s"*** WARNING: SUSPICIOUS SPEED!!! $currentSpeed")
          }
          //else {

            acceleration = currentSpeed - speed


            speed = currentSpeed
            force = currentPiece match {
              case cp:CurvedPiece => (speed * speed) / cp.radiusOfLane(position.piecePosition.lane.endLaneIndex)
              case _ => acceleration
            }

            if (isCorner(currentPiece) && position.angle.abs > 1.0 && position.angle.abs < minForceAngle.abs) {
              minForceAngle = minForceAngle.min(position.angle.abs)
              val angle = position.angle
              println(s"Slippage $force first detected with angle $angle")
            }


            if (!turboOn) {
              maxSpeed = maxSpeed.max(speed)
              maxAcceleration = maxAcceleration.max(acceleration)

              if (acceleration < 0) {
                maxDeceleration = maxDeceleration.max(acceleration.abs)
              }
            }
          //}
            //maxDeceleration = maxAcceleration // not sure if the deceleration is helping, so ignore for now
          //}
          //log.info(s"Current speed is $speed")

          val av = position.angle - lastPosition.angle
          slipAcceleration = av - slipVelocity
          slipVelocity = av

          if (!crashed) {
                      var piece = Track.pieces(position.piecePosition.pieceIndex)
                      piece match {
                        case cp: CurvedPiece =>
                          val av = angularVelocity
                          angularVelocity = (currentSpeed / piece.lengthOfLane(position.piecePosition.lane.endLaneIndex)) * cp.angle
                          angularAcceleration = av - angularVelocity
                          val centrifugalForce = (slipVelocity * slipVelocity) / (dimensions.length - dimensions.guideFlagPosition)
                          //log.info(s"CentrigualForce = $centrifugalForce")
                          //log.info(s"Angular velocity is $angularVelocity degrees")
                        case _ => angularVelocity = 0.0
                      }
                    }


        }






        var ticksUntilCrashAtVelocity = if (slipVelocity != 0) (60.0 - position.angle.abs) / slipVelocity.abs else 100

        if (ticksUntilCrashAtVelocity < 10) {
          log.info(s"** Anticipated crash in $ticksUntilCrashAtVelocity. Current angular velocity  = $slipVelocity")
        }
        if (slipVelocity.abs > 1.0) {
          var ticks = 0
          var estimatedAngle = position.angle

          while (ticks < 10 && estimatedAngle.abs < 60.0) {
            ticks = ticks + 1
            estimatedAngle = estimatedAngle + (ticks * slipAcceleration)
          }
          if (estimatedAngle.abs >= 60) {
            log.info(s"*** Anticipated crash ($estimatedAngle) due to acceleration in $ticks ticks")
          }
        }


        val nextPiece = currentPiece.next

        throttle = initialThrottle + (position.piecePosition.lap * 0.05)

        if (lastPosition != null && lastPosition.piecePosition.pieceIndex != position.piecePosition.pieceIndex) {
          dump()
        }

        lastPosition = position


        car ! SetThrottle(throttle)
      targetMonitor ! (position -> speed)
    }

    def isCorner(piece:Piece) = {
      piece match {
        case c:CurvedPiece => true
        case _ => false
      }
    }


  def dump() {
    val index = lastPosition.piecePosition.pieceIndex
    val angle = lastPosition.angle
    val piece = Track.pieces(index)
    val curve = piece match {
      case c:CurvedPiece => c.angle
      case _ => 0
    }

    val r = dimensions.length - dimensions.guideFlagPosition
    val slipForce = Math.pow(Math.toRadians(slipVelocity) * r, 2.0 ) / r
    val totalForce = acceleration + slipForce + force

    println(s"P $index curve:$curve ttl: $throttle: speed=$speed  accel=$acceleration  angle=$angle slipForce=$slipForce force=$force totalForce=$totalForce")
  }


}
