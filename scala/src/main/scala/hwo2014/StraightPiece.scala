package hwo2014

/**
 * Created by ed on 16/04/14.
 */

object StraightPiece {
  val DEFAULT_TARGET = 50.0

}


class StraightPiece(l: Double) extends Piece {
  val length: Double = l

  def apply(l:List[LaneDataMsg]) {
    lanes = l.map(laneMsg => new Lane(laneMsg.index, length, laneMsg.distanceFromCenter))
    lanes.foreach(l => l.target.speed = StraightPiece.DEFAULT_TARGET)

    lanes = lanes.sortWith((l0,l1) => l0.index < l1.index)
  }

  override def describe = if (switch) "Switch " + length else "Straight " + length
}
