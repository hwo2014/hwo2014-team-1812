package hwo2014

import akka.actor.{ActorLogging, Actor}
import scala.concurrent.duration.Duration
import java.util.Date
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global
import java.text.SimpleDateFormat

/**
 * Created by beisenmenger on 16/04/2014.
 */

case class Replay()

class BlackBox(bufferSize:Duration) extends Actor with ActorLogging {
  val df = new SimpleDateFormat("HH:mm:ss.SS")
  var messages = List[RecordedMessage]()

  //context.system.scheduler.schedule(5 seconds, 5 seconds, context.self, "Size")

  override def receive = {
    case "Size" => println(messages.size + " messages in black box")
    case m:RaceMessage =>
      val now = System.currentTimeMillis()
      messages = RecordedMessage(now, m) :: messages

      val deadline = now - bufferSize.toMillis
      messages = messages.reverse.dropWhile(_.timestamp < deadline).reverse

      /*m match {
        case x:CarPositions =>
        case _ => println(m)
      }
      */
//      log.info("Blackbox size: " + messages.length)
    case Replay() =>
      println("----------  Replay ----------------")
      messages.reverse.foreach(m => println(df.format(new Date(m.timestamp)) + "\t" + m.message ))
    case x@_ =>
      log.info("Unknown blackbox message: " + x)
  }

  case class RecordedMessage(timestamp:Long,  message:RaceMessage)
}
