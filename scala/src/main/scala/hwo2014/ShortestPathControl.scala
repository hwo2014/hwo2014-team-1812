package hwo2014

import akka.actor.ActorRef

/**
 * Created by ed on 23/04/14.
 */
class ShortestPathControl(car: ActorRef) extends PathControl(car) {

  override def defineOptimalLane(myPosition: CarPosition, otherCarPositions: List[CarPosition], switchPiece: Piece): Option[String] = {
    var defaultDirection: String = "None"
    val switchPieceIndex = Track.pieces.indexOf(switchPiece)
    val currentLaneIndex = myPosition.piecePosition.lane.endLaneIndex
    if (Track.shortestPath.isDefined) {
      val shortestPath = Track.shortestPath.get
      val targetLane = shortestPath.lanes(switchPieceIndex + 1)
      if (targetLane.index < currentLaneIndex) {
        defaultDirection = "Left"
      } else if (targetLane.index > currentLaneIndex) {
        defaultDirection = "Right"
      }
    }
    Option.apply(defaultDirection)
  }
}
