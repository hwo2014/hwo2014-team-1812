package hwo2014

import akka.actor.{ActorLogging, Actor}

/**
 * Created by beisenmenger on 20/04/2014.
 */
class PieceTargetMonitor extends Actor with ActorLogging {
  var currentPiece = -1
  var currentLane = -1
  var maxSpeed = 0.0
  var maxSlippage = 0.0
  var crash = false

  override def receive = {
    case c:Crash => crash = true
    case (cp:CarPosition, speed:Double) =>
      if (cp.piecePosition.pieceIndex != currentPiece) {
        if (currentPiece != -1) {
          Track.pieces(currentPiece).history = History(speed, cp.angle, currentLane)
          updateTarget(currentPiece, currentLane, maxSpeed, maxSlippage, crash)
          maxSpeed = 0.0
          maxSlippage = 0.0
          crash = false
        }
      }

    currentPiece = cp.piecePosition.pieceIndex
    currentLane = cp.piecePosition.lane.endLaneIndex
    maxSpeed = maxSpeed.max(speed)
    maxSlippage = maxSlippage.max(cp.angle.abs)

    case gi:GameInit =>

          var sections:List[Section] = Track.pieces.map(initialSectionFor)

      //the sections are only used here currently to initialize the targets
          sections = mergeSections(sections)

       // println(sections.map(_.describe).mkString("Sections:\n", "\n", ""))

      for {s <- sections
                 p <- s.pieces}
            {
              s.initializeTargets
            }
  }

  def mergeSections(s:List[Section]):List[Section] = {
      s match {
        case x :: Nil => List(x)
        case x :: xs =>
          val xsmerged = mergeSections(xs)
          if (x.canMergeWith(xsmerged.head)) {
            x.merge(xsmerged.head) :: xsmerged.tail
          } else {
            x :: xsmerged
          }
      }
    }

    def initialSectionFor(piece:Piece) = {
      piece match {
        case sp:StraightPiece => new Straight(sp)
        case cp:CurvedPiece => new Turn(cp)
      }
    }

  def updateTarget(piece:Int, lane:Int, fastest:Double, slip:Double, crash:Boolean) {

    if (crash) {
      //for (i <- 0 until 3) {
        var p = Track.pieces(piece)

        var target = p.lanes(lane).target.speed * 0.85
      log.info(s"Target after crash is $target for piece $piece")
      p = p.previous
      while (p.history.angle.abs > 5) {
        target = p.lanes(p.history.lane).target.speed * 0.85
        p.lanes(p.history.lane).target.speed = target
        log.info(s"Target after crash is $target for piece $piece")
        p = p.previous
      }
      //}
    }
    else if (slip < 35.0) {
      val percentFromMaxSlippage = slip / 30.0
      val increase = (1.0 - percentFromMaxSlippage) / 4.0
      val currentTarget = Track.pieces(piece).lanes(lane).target.speed
      val newTarget = currentTarget * (1.0 + increase)
      log.info(s"New faster target speed for piece $piece is $newTarget")
      Track.pieces(piece).lanes(lane).target.speed = newTarget
    } else if (slip > 55.0) {
      val currentTarget = Track.pieces(piece).lanes(lane).target.speed
      val newTarget = currentTarget * (1.0 - (1.0 - (slip/45.0)).abs)
      log.info(s"New slower target speed for piece $piece is $newTarget")
      Track.pieces(piece).lanes(lane).target.speed = newTarget

      var p = Track.pieces(piece).previous
      while (p.history.angle.abs > 5) {
//        val target = p.lanes(p.history.lane).target.speed * 0.6667
        val target = p.lanes(p.history.lane).target.speed * (1.0 - (1.0 - (slip/45.0)).abs)
        p.lanes(p.history.lane).target.speed = target
        log.info(s"New slower target speed  $target for previous piece")
        p = p.previous
      }
    } else {
      val target = Track.pieces(piece).lanes(lane).target.speed
      log.info(s"Target is still $target for piece $piece")
    }
  }
}
