package hwo2014

import akka.actor.{Props, ActorLogging, Actor, ActorRef}

/**
 * Created by beisenmenger on 19/04/2014.
 */

case class Tick()
case class Speed(value:Double)
case class SetThrottle(value:Double)
case class SwitchLane(value: String)
case class LimitThrottle(limit: Double)
case class Lap(current:Int, total:Int)

class Engine(client:ActorRef) extends Actor with ActorLogging {

  val car = context.actorOf(Props(new Car(client)), "car")
  val throttleControl = context.actorOf(Props(new ForceThrottleControl(car)), "throttle")
  //val throttleControl = context.actorOf(Props(new ThrottleDiscoveryActor(car)), "throttle")
  val pathControl = context.actorOf(Props(new ShortestPathControl(car)), "steering")
  val turboActor = context.actorOf(Props(new TurboActor(car)), "turbo")

  var laps = 0
  var currentLap = 1

  override def receive = {
    case gi:GameInit =>
      log.info("Initializing track")
      Track.init(gi)

      for (i <- 0 until Track.lanes) {

        val totalLength = Track.pieces.foldLeft(0.0)((s,p) => s + p.lengthOfLane(i))
        log.info(s"Total length of lane $i is $totalLength" )
      }
      val possiblePathsSize = Track.possiblePaths.size
      log.info(s"Number of possible paths : $possiblePathsSize")
      if (Track.shortestPath.isDefined) {
        val shortestDistance = Track.shortestPath.get.length
        log.info(s"Shortest path distance : $shortestDistance")
      }

      gi.data.race.raceSession.laps match {
        case Some(l) => laps = l
        case _ =>
      }
      //println(Track.describe)
      throttleControl ! gi
      throttleControl ! gi.data.race.cars(0).dimensions
      throttleControl ! Lap(currentLap, laps)
      log.info(s"Lap $currentLap")
    case cp:CarPositions =>
      val filteredList = cp.data.filter(pos => pos.id.color == Client.myColour)
      if (!(filteredList.isEmpty)) {
        val currentPosition = filteredList(0)
        throttleControl ! currentPosition
        pathControl ! cp
        car ! Tick()
      }
      turboActor ! cp
    case c:Crash =>
      throttleControl ! c
      car ! Tick()
    case s:Spawn =>
      throttleControl ! s
      car ! Tick()
    case ta:TurboAvailable =>
      //log.info("***** TURBO AVAILABLE *****")
      throttleControl ! ta
      turboActor ! ta
    case t:TurboStart =>
      throttleControl ! t
      turboActor ! t
    case t:TurboEnd =>
      throttleControl ! t
      turboActor ! t
    case t: LimitThrottle => car ! t
    case lf:LapFinished =>
      if (lf.data.car.color == Client.myColour) {
        currentLap = lf.data.lapTime.lap + 2   // lap is 0-based and we want the next one
        throttleControl ! Lap(currentLap, laps)
        log.info(s"Lap $currentLap")
      }
    case _ =>  car ! Tick()
  }

  def setThrottleLimit(limit: Double) = car.asInstanceOf[Car].throttleLimit = Option.apply(limit)
}


class Car(client:ActorRef) extends Actor with ActorLogging {
  var currentThrottle = 0.0
  var switchDirection: Option[String] = Option.empty
  var throttleLimit: Option[Double] = Option.empty
  var turbo:Option[Turbo] = None

  override def receive = {
    case Tick() =>
      switchDirection match {
        case Some(value) =>
          client ! Switch("switchLane", value)
          switchDirection = Option.empty
        case None =>
          turbo match {
            case Some(t) => client ! t
            case _ => client ! Throttle("throttle", currentThrottle)
          }

      }
    case SetThrottle(value) =>
      throttleLimit match {
        case Some(limit: Double) if limit > value => currentThrottle = value
        case Some(limit: Double) if limit < value => currentThrottle = limit
        case None => currentThrottle = value
      }
    case t:Turbo => turbo = Some(t)
    case SwitchLane(value) => switchDirection = Option.apply(value)
    case t:Replay => client ! t
    case LimitThrottle(value) => throttleLimit = Option.apply(value)
  }
}
