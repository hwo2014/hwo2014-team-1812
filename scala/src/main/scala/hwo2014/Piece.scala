package hwo2014

/**
 * Created by ed on 16/04/14.
 */

case class History(speed:Double, angle:Double, lane:Int)

abstract class Piece {

  var lanes : List[Lane] = Nil

  var next : Piece = null

  var previous : Piece = null

  var switch: Boolean = false

  var friction = Track.friction

  def next(piece:Piece) {
    next = piece
    next.previous = this
  }

  def apply(l:List[LaneDataMsg])

  def lengthOfLane(i:Int) = lanes(i).length

  def canSwitch : Boolean = switch

  var history:History = null

  def describe:String
}
