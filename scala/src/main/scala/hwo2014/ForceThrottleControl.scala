package hwo2014

import akka.actor.{Props, ActorLogging, Actor, ActorRef}

/**
 * Created by beisenmenger on 23/04/2014.
 */
class ForceThrottleControl(car:ActorRef) extends Actor with ActorLogging {
  var lastPosition:CarPosition = null
    var speed = 0.0   // distance units per tick
    var acceleration = 10.0
    var maxSpeed = 0.0
    var maxAcceleration = 0.0
    var maxDeceleration = 1.0

  var slipVelocity = 0.0
  var slipAcceleration = 0.0

  var throttle = 0.5

  var targetMonitor = context.actorOf(Props(new ForceTargetMonitor()), "targetMonitor")
  var turboAvailable = false
  var turboOn = false
  var turboInfo:TurboAvailable = null
  var crashed = false

  var dimensions:CarDimensionsMsg = null

  val pid = new PID()

  var lap:Lap = null


  override def receive = {
    case l:Lap => lap = l
    case cd:CarDimensionsMsg =>
      dimensions = cd
      targetMonitor ! cd
    case c:Crash =>
      targetMonitor ! c
      crashed = true
    case s:Spawn => crashed = false
    case gi:GameInit =>
      targetMonitor ! gi
      pid.Kp = 0.25
      pid.Kd = 0.3
      pid.Ki = 0
    case ta:TurboAvailable =>
      turboAvailable = true
      turboInfo = ta
    case t:TurboStart =>
      turboOn = true
      turboAvailable = false
    case t:TurboEnd =>
      turboOn = false
      throttle = 0.1
    case position:CarPosition =>

      if (lastPosition != null) {
        val currentSpeed = Track.distanceBetween(lastPosition.piecePosition, position.piecePosition)

        if (!turboOn) {
          acceleration = currentSpeed - speed
          speed = currentSpeed
          maxSpeed = maxSpeed.max(speed)
            maxAcceleration = maxAcceleration.max(acceleration)

            if (acceleration < 0) {
              maxDeceleration = maxDeceleration.max(acceleration.abs)
            }
          }


        val av = position.angle - lastPosition.angle
        slipAcceleration = av - slipVelocity
        slipVelocity = av


      }
      lastPosition = position



      val currentPiece = Track.pieces(position.piecePosition.pieceIndex)
      val nextPiece = currentPiece.next

      var targetSpeed = nextPiece.lanes(position.piecePosition.lane.endLaneIndex).target.speed

      var breakingDistance = 20 + ((speed - targetSpeed) / maxDeceleration)
      var targetPosition = nextPiece
      var distanceToTargetSpeed = Track.distanceBetween(lastPosition.piecePosition, position.piecePosition)

      while (breakingDistance > distanceToTargetSpeed) {
        distanceToTargetSpeed = distanceToTargetSpeed + targetPosition.lengthOfLane(position.piecePosition.lane.endLaneIndex)
        targetPosition = targetPosition.next
        targetSpeed = targetPosition.lanes(position.piecePosition.lane.endLaneIndex).target.speed
      }



      var speedDiff = targetSpeed - speed
      val speedDiffPercent = (speedDiff / targetSpeed).abs


      /*if (isCorner(currentPiece)) {

        if (position.angle > 0) {
          val cp = currentPiece.asInstanceOf[CurvedPiece]
          val piece = position.piecePosition.pieceIndex
          val turn = cp.angle
          val lane = position.piecePosition.lane.endLaneIndex
          val radius = cp.radiusOfLane(lane)
          val angle = position.angle
          val centripetalForce = Forces.centripetalForce(speed, cp.radiusOfLane(position.piecePosition.lane.endLaneIndex))
          val forceSupplied = acceleration
          val forceSuppliedInDirectionOfCentre = acceleration * Math.sin(Math.toRadians(position.angle))
          val slipForce = Forces.centripetalForce(Math.toRadians(slipVelocity), dimensions.length - dimensions.guideFlagPosition)
          val targetForce = cp.friction
          //val targetSpeed = cp.lanes(lane).target.speed
          val forwardMomentum = speed * (Math.sin(Math.toRadians(angle)))


//          println(s"Piece $piece, Angle: $turn  Radius $radius,    speed: $speed    centripetal force: $centripetalForce")
//          println(s"Angle: $angle A.Velocity: $slipVelocity Slip Force: $slipForce")
//          println(s"Target Force: $targetForce    Target Speed: $targetSpeed   Momentum: $forwardMomentum")
//          println(s"Throttle $throttle    acceleration $acceleration   % speed diff: $speedDiffPercent")
//          if ( angle < 1.0) {
//            val estimatedFriction = centripetalForce + forwardMomentum - slipForce
//            println(s"Estimated Friction: $estimatedFriction \n\n")
//          } else {
//            println("\n")
//          }

//          if (angle > 35.0) {
//            val deltaF = slipForce
//            val deltaV = Forces.velocityForForce(deltaF, radius) * 1.5
//            targetSpeed = targetSpeed - deltaV
//            println(s"Adjusting force by $deltaF and speed by $deltaV to $targetSpeed")
//          }
        }
      }*/


      if (pid != null) {
        pid.setpoint(targetSpeed)
        throttle = pid.calculate(speed).max(0.01).min(1.0)
      } else {

        if (speedDiff > 0) {
          // below speed
          val max = currentPiece match {
            case s: StraightPiece => 1.0
            case _ => 1.0
          }
          throttle = (throttle * (1.0 + speedDiffPercent)).min(max)
        } else if (speedDiff < 0) {
          // above speed

          throttle = (throttle * (1.0 - speedDiffPercent)).max(0.005)
        }
      }


      if (!crashed && turboAvailable && isSuitableForTurbo(position, speed, breakingDistance)) {
        log.info("TURBO is go")
        car ! Turbo("turbo", "Yeah baby!")
        turboAvailable = false
      }

        car ! SetThrottle(throttle)
    targetMonitor ! (position -> speed)
  }

  def isCorner(piece:Piece) = {
    piece match {
      case c:CurvedPiece => true
      case _ => false
    }
  }

  def isFinalLap = lap.current == lap.total

  def isSuitableForTurbo(position:CarPosition, speed:Double, breakingDistance:Double) = {
//    val l = lap.current
//    val total = lap.total
//    val finalStraight = Track.isOnFinalStraight(position.piecePosition.pieceIndex)
//    val piece = position.piecePosition.pieceIndex
//    println(s"Piece $piece lap $l/$total  Final Straight ? $finalStraight")


    if (isFinalLap && Track.isOnFinalStraight(position.piecePosition.pieceIndex)) {
      true
    } else {

      if (position.angle.abs > 20 || speed < 2.0) false
      else {
        val turboTicks = turboInfo.data.turboDurationTicks.get

        val distanceRequired = (breakingDistance * 3) + turboTicks * (speed * turboInfo.data.turboFactor)

        var piece = Track.pieces(position.piecePosition.pieceIndex)

        var distanceAvailable = piece.lengthOfLane(position.piecePosition.lane.endLaneIndex) - position.piecePosition.inPieceDistance
        piece = piece.next
        while (!isCorner(piece)) {
          distanceAvailable = distanceAvailable + piece.lengthOfLane(position.piecePosition.lane.endLaneIndex)
          piece = piece.next
        }

        // todo Should always do it on the final straight of the last lap if possible
        distanceAvailable > distanceRequired + 50
        false
      }
    }
  }

}
